#include "statystyka.h"
#include <math.h>

float odchylenie(float tab[], int rozmiar) 
{
    float od=0;
    float sr = srednia(tab, rozmiar);
    for (int i = 0; i < rozmiar; i++)
    {
        od += (tab[i] - sr) * (tab[i] - sr);
    }

    od = sqrt(od / rozmiar);

    return od;
}

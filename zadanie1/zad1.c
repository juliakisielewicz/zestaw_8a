/* 1. Napisać program, który wczyta plik P0001_attr.rec z mojej strony 
i utworzy 3 tablice dynamiczne floatów, do których zaczyta wartości X, Y i RHO. 
Zostanie policzona i wyświetlona średnia, mediana i odchylenie standardowe dla X, Y i RHO. 
Wyniki statystyk zostaną dopisane do pierwotnego pliku. W modułach mają znaleźć się tylko funkcje/procedury dla statystyk. */

#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "statystyka.h"

int main(void)
{
    FILE *plik = NULL;
    int n = 50, i = 0;
    
    float *tab_x; 
    tab_x = (float *)malloc(n*sizeof(float));
   
    float *tab_y; 
    tab_y = (float *)malloc(n*sizeof(float));
   
    float *tab_RHO; 
    tab_RHO = (float *)malloc(n*sizeof(float));

    char tab_tmp[55]=""; 
   
    plik = fopen("P0001_attr.rec", "r");

    for(i = 0; i < 4; i++)
    {
        fscanf(plik, "%s", tab_tmp);
    }

    for(i = 0; i < n; i++)
    {
        fscanf(plik, "%s %f %f %f", tab_tmp, &tab_x[i], &tab_y[i], &tab_RHO[i]);
    }

    fclose(plik);

    plik = fopen("P0001_attr.rec", "a");
    
    fprintf(plik, "\n tablica       X              Y            RHO");
    fprintf(plik, "\n srednia      %f     %f     %f", srednia(tab_x, n), srednia(tab_y, n), srednia(tab_RHO, n));
    fprintf(plik, "\n mediana      %f     %f     %f", mediana(tab_x, n), mediana(tab_y, n), mediana(tab_RHO, n));
    fprintf(plik, "\n odchylenie   %f     %f     %f", odchylenie(tab_x, n), odchylenie(tab_y, n), odchylenie(tab_RHO, n));

    printf("\n tablica       X              Y            RHO");
    printf("\n srednia      %f     %f     %f", srednia(tab_x, n), srednia(tab_y, n), srednia(tab_RHO, n));
    printf("\n mediana      %f     %f     %f", mediana(tab_x, n), mediana(tab_y, n), mediana(tab_RHO, n));
    printf("\n odchylenie   %f     %f     %f", odchylenie(tab_x, n), odchylenie(tab_y, n), odchylenie(tab_RHO, n));

    fclose(plik);

    free(tab_x);
    free(tab_y);
    free(tab_RHO);


   return 0; 
}
#include "statystyka.h"

float mediana(float tab[], int rozmiar)

{
    float temp;
    float wynik;
    for (int i = 1; i < rozmiar; i++)
	{
		for (int j = i-1; j>=0; j--)
		{
			if (tab[i] < tab[j])
			{
				temp = tab[i];
				tab[i] = tab[j];
				tab[j] = temp;

				i--;
			}
		}	
    }

    if(rozmiar%2==0)
    {
        wynik = (tab[(rozmiar-1)/2]+tab[1+(rozmiar-1)/2])/2.0;
    }   
    else
    {
        wynik = tab[rozmiar/2];
    }
    
    return wynik;
}
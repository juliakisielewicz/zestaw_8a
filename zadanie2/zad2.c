/* 2. Napisać program, który zrobi to samo co program pierwszy, ale zapisze zaczytane
dane oraz wyniki do struktury. Program skorzysta z modułów do statystyk opisowych.
Program sprawdzi, czy w pliku są już dopisane statystyki i jeśli ich nie ma, dopisze je.
W modułach mają znaleźć się tylko funkcje/procedury dla statystyk, ewentualnie definicja struktury */
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "statystyka.h"

typedef struct
{
    float *dane;
    float sr;
    float med;
    float od; 
}stat;

void wyniki(stat *struktura, int rozmiar);

int main(void)
{
    FILE *plik = NULL;
    int n = 50, i = 0;
    stat x;
    stat y;
    stat RHO;
    char tmp[55]="";

    x.dane = (float *)malloc(n*sizeof(float));
    y.dane = (float *)malloc(n*sizeof(float));
    RHO.dane = (float *)malloc(n*sizeof(float));
    

    plik = fopen("P0001_attr.rec", "r");

    for(i = 0; i < 4; i++)
    {
        fscanf(plik, "%s", tmp);
    }
    
    for(i = 0; i < n; i++)
    {
        fscanf(plik, "%s %f %f %f", tmp, &x.dane[i], &y.dane[i], &RHO.dane[i]);
        printf("%.5f %.5f %.3f\n",  x.dane[i], y.dane[i], RHO.dane[i]);
    }

    wyniki(&x, n);
    wyniki(&y, n);
    wyniki(&RHO, n);
    
    printf("\n tablica       X              Y            RHO");
    printf("\n srednia      %f     %f     %f", x.sr, y.sr, RHO.sr);
    printf("\n mediana      %f     %f     %f", x.med, y.med, RHO.med);
    printf("\n odchylenie   %f     %f     %f", x.od, y.od, RHO.od);

    int obecnosc = 0;
    if((fgetc(plik)) != EOF)
    {
        obecnosc++;
    }
    
    fclose(plik);

    if(obecnosc == 0)
    {
        plik = fopen("P0001_attr.rec", "a");
        fprintf(plik, "\n tablica       X              Y            RHO");
        fprintf(plik, "\n srednia      %f     %f     %f", x.sr, y.sr, RHO.sr);
        fprintf(plik, "\n mediana      %f     %f     %f", x.med, y.med, RHO.med);
        fprintf(plik, "\n odchylenie   %f     %f     %f", x.od, y.od, RHO.od);  
        fclose(plik);
    }
        
    return 0; 
}

void wyniki(stat *struktura, int rozmiar)
{
    struktura->sr = srednia(struktura->dane, rozmiar);
    struktura->med = mediana(struktura->dane, rozmiar);
    struktura->od = odchylenie(struktura->dane, rozmiar);

    return;
}